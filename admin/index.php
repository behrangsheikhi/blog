<?php
require_once '../function/helpers.php';
require_once '../function/pdo_connection.php';
require_once '../function/check-login.php';

?>

<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>پنل مدیریت</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css')  ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css')  ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css')  ?>">

</head>

<body>
    <section id="app">

        <!-- admin panel header here -->
        <?php require_once 'layouts/top-nav.php'; ?>

        <section class="container-fluid">
            <section class="row">
                <section class="col-md-2 p-0">
                    <!-- admin panel sidebar here -->
                    <?php require_once 'layouts/sidebar.php'; ?>
                </section>
                <section class="col-md-10 pb-3">

                    <section style="min-height: 80vh;" class="d-flex justify-content-center align-items-center">
                        <section>
                            <h1>داشبورد</h1>
                            <ul class="mt-2 li">
                                <li>
                                    <h3>ارتباط با دیتابیس</h3>
                                </li>
                                <li>
                                    <h3>بارگزاری عکس</h3>
                                </li>
                                <li>
                                    <h3>بلاگ ( دسته بندی و پست)</h3>
                                </li>
                            </ul>
                        </section>
                    </section>

                </section>
            </section>
        </section>


    </section>

    <script src="<?= asset('asset/js/jquery.min.js') ?>"></script>
    <script src="<?= asset('asset/js/bootstrap.min.js')  ?>"></script>
</body>

</html>