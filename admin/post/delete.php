<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

global $connect;

if (isset($_GET['post_id']) and $_GET['post_id'] != '') {
    $query = 'SELECT * FROM blog.posts WHERE id = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$_GET['post_id']]);
    $post = $statement->fetch();

    if ($post === false) {
        redirect('admin/post');
    } else {
        $basePath = dirname(__DIR__, 2);
        // remove post image
        if (file_exists($basePath . $post->image)) {
            unlink($basePath . $post->image);
        }
        // remove the post itself
        $query = 'DELETE FROM blog.posts WHERE id =? ';
        $statement = $connect->prepare($query);
        $statement->execute([$_GET['post_id']]);
    }
}
redirect('admin/post');