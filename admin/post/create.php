<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';


if (
    isset($_POST['title']) and $_POST['title'] !== ''
    and isset($_POST['cat_id']) and $_POST['cat_id'] !== ''
    and isset($_POST['body']) and $_POST['body'] !== ''
    and isset($_FILES['image']) and $_FILES['image']['name'] !== ''
) {
    global $connect;

    $query = 'SELECT * FROM blog.categories WHERE id = ? ;';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['cat_id']]);
    $category = $statement->fetch();
    if ($category === false) {
        redirect('admin/post');
    }
    // check if the format of image is ok or not
    $allowedMimes = ['png', 'jpg', 'jpeg', 'gif'];
    $imageMime = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
    if (!in_array($imageMime, $allowedMimes)) {
        redirect('admin/post');
    }
    $basePath = dirname(dirname(__DIR__));
    $image = '/asset/images/posts/' . date('y_m_d_H_i_s') . '.' . $imageMime;
    $image_upload = move_uploaded_file($_FILES['image']['tmp_name'], $basePath . $image);
    if ($category !== false and $image_upload !== false) {
        $query = 'INSERT INTO blog.posts SET title = ? , cat_id = ? , body = ? , image = ? , created_at = NOW();';
        $statement = $connect->prepare($query);
        $statement->execute([$_POST['title'], $_POST['cat_id'], $_POST['body'], $image]);
    }
    redirect('admin/post');
}

?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>ایجاد پست</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>">
</head>

<body>
    <section id="app">
        <?php require_once '../layouts/top-nav.php'; ?>
        <section class="container-fluid">
            <section class="row">
                <section class="col-md-2 p-0">
                    <?php require_once '../layouts/sidebar.php'; ?>
                </section>

                <section class="col-md-10 pt-3">
                    <h3 class="page-title">
                        ایجاد پست
                    </h3>
                    <form class="add-post-form" action="<?= url('admin/post/create.php') ?>" method="post" enctype="multipart/form-data">
                        <div class="row form-row">
                            <section class="form-group">
                                <label for="title">عنوان</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="عنوان پست . . .">
                            </section>
                            <section class="form-group">
                                <label for="image">تصویر</label>
                                <input type="file" class="form-control" name="image" id="image">
                            </section>
                        </div>
                        <div class="row form-row">
                            <section class="form-group">
                                <label for="cat_id">دسته بندی</label>
                                <select class="form-control" name="cat_id" id="cat_id">
                                    <option value="" >دسته بندی را وارد کنید</option>
                                    <?php
                                    global $connect;
                                    $query = 'SELECT * FROM blog.categories;';
                                    $statement = $connect->prepare($query);
                                    $statement->execute();
                                    $categories = $statement->fetchAll();
                                    foreach ($categories as $category) {
                                    ?>
                                        <option style="font-size: 12px" value="<?= $category->id; ?>"><?= $category->name; ?></option>
                                    <?php } ?>
                                </select>
                            </section>
                            <section class="form-group">
                                <label for="body">بدنه</label>
                                <textarea class="form-control" style="padding: 15px" name="body" id="body" rows="10" placeholder="متن پست را وارد کنید . . ."></textarea>
                            </section>
                        </div>

                        <section class="form-group btn-box float-right">
                            <button type="submit" class="btn btn-primary m-1" name="submit">ایجاد</button>
                            <button type="submit" class="btn btn-danger m-1" name="cancel" value="<?= url('admin/post') ?>">انصراف</button>
                        </section>
                    </form>

                </section>
            </section>
        </section>

    </section>

    <script src="../../asset/js/jquery.min.js"></script>
    <script src="../../asset/js/bootstrap.min.js"></script>
</body>

</html>