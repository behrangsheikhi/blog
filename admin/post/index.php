<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>پنل مدیریت</title>
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/icons-regular-rounded.css') ?>">
</head>

<body>
    <section id="app">
        <?php require_once '../layouts/top-nav.php'; ?>

        <section class="container-fluid">
            <section class="row">
                <section class="col-md-2 p-0">
                    <?php require_once '../layouts/sidebar.php'; ?>
                </section>
                <section class="col-md-10 pt-3">

                    <section class="mb-2 d-flex justify-content-between align-items-center">
                        <h3 class="page-title">
                            پست ها
                        </h3>
                        <a href="<?= url('admin/post/create.php'); ?>" class="btn btn-sm btn-success">ایجاد پست</a>
                    </section>

                    <section class="table-responsive">
                        <table class="table table-striped table-">
                            <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>تصویر</th>
                                    <th>عنوان</th>
                                    <th>دسته بندی</th>
                                    <th>بدنه</th>
                                    <th>وضعیت</th>
                                    <th>تنظیمات</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                global $connect;
                                $query = 'SELECT blog.posts.*,blog.categories.name AS category_name FROM blog.posts LEFT JOIN blog.categories ON blog.posts.cat_id = blog.categories.id ;';
                                $statement = $connect->prepare($query);
                                $statement->execute();
                                $posts = $statement->fetchAll();
                                foreach ($posts as $post) {
                                ?>
                                    <tr>
                                        <td><?= $post->id; ?></td>
                                        <td><img alt="post image" style="width: 90px;height : 90px;" src="<?= asset($post->image); ?>"></td>
                                        <td><?= $post->title; ?></td>
                                        <td><?= $post->category_name; ?></td>
                                        <td><?= substr($post->body, 0, 30) . '. . . '; ?></td>
                                        <td>
                                            <?php if ($post->status == 1) { ?>
                                                <span class="text-success">فعال</span>
                                            <?php } else { ?>
                                                <span class="text-danger">غیر فعال</span>
                                            <?php } ?>
                                        </td>
                                        <td class="btn-box">
                                            <a href="<?= url('admin/post/change-status.php?post_id=').$post->id; ?>" class="btn btn-warning btn-sm">تغییر وضعیت</a>
                                            <a href="<?= url('admin/post/edit.php?post_id=').$post->id; ?>" class="btn btn-info btn-sm">ویرایش</a>
                                            <a href="<?= url('admin/post/delete.php?post_id=').$post->id; ?>" class="btn btn-danger btn-sm">حذف</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </section>


                </section>
            </section>
        </section>





    </section>

    <script src="../../asset/js/jquery.min.js"></script>
    <script src="../../asset/js/bootstrap.min.js"></script>
</body>

</html>