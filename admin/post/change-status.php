<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

global $connect;

if (!isset($_GET['post_id']) or $_GET['post_id'] == '') {
    redirect('admin/post/create.php');
}

$query = 'SELECT * FROM blog.posts WHERE id = ? ';
$statement = $connect->prepare($query);
$statement->execute([$_GET['post_id']]);
$post = $statement->fetch();

if ($post !== false) {
    $status = ($post->status == 1) ? 0 : 1;
    $query = 'UPDATE blog.posts SET status = ? WHERE id =?';
    $statement = $connect->prepare($query);
    $statement->execute([$status, $_GET['post_id']]);

}
redirect('admin/post/edit.php');