<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

global $connect;

if (!isset($_GET['post_id'])) {
    redirect('admin/post');
}


$query = 'SELECT * FROM blog.posts WHERE id = ? ;';
$statement = $connect->prepare($query);
$statement->execute([$_GET['post_id']]);
$post = $statement->fetch();
if ($post === false) {
    redirect('admin/post');
}

if (
    isset($_POST['title']) and $_POST['title'] !== ''
    and isset($_POST['cat_id']) and $_POST['cat_id'] !== ''
    and isset($_POST['body']) and $_POST['body'] !== ''
) {
    // check if the category exists or not?
    $query = 'SELECT * FROM blog.categories WHERE id = ? ;';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['cat_id']]);
    $category = $statement->fetch();

    if (isset($_FILES['image']) and $_FILES['image']['name'] !== '') {
        $allowedMimes = ['png', 'jpg', 'jpeg', 'gif'];
        $imageMime = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
        if (!in_array($imageMime, $allowedMimes)) {
            redirect('admin/post');
        }
        $basePath = dirname(__DIR__, 2);
        if (file_exists($basePath . $post->image)) {
            unlink($basePath . $post->image);
        }
        $image = '/asset/images/posts/' . date("y_m_d_H_i_s") . '.' . $imageMime;
        $image_upload = move_uploaded_file($_FILES['image']['tmp_name'], $basePath . $image);

        if ($category !== false and $image_upload !== false) {
            $query = 'UPDATE blog.posts SET title = ?,cat_id = ? ,body = ?,image = ?,updated_at = NOW() WHERE id = ? ;';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['title'], $_POST['cat_id'], $_POST['body'], $image, $_GET['post_id']]);
        }
    } else {
        if ($category !== false) {
            $query = 'UPDATE blog.posts SET title = ?,cat_id = ?,body = ?,updated_at = NOW() WHERE id = ?;';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['title'], $_POST['cat_id'], $_POST['body'], $_GET['post_id']]);
        }
    }
    redirect('admin/post');
}


?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>ویرایش پست</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>">
</head>

<body>
<section id="app">

    <?php require_once '../layouts/top-nav.php'; ?>
    <section class="container-fluid">
        <section class="row">
            <section class="col-md-2 p-0">
                <?php require_once '../layouts/sidebar.php'; ?>
            </section>
            <section class="col-md-10 pt-3">
                <h3 class="page-title">
                    ویرایش پست
                </h3>
                <form action="<?= url('admin/post/edit.php?post_id=') . $_GET['post_id'] ?>" class="add-post-form"
                      method="post" enctype="multipart/form-data">
                    <div class="row form-row">
                        <section class="form-group">
                            <label for="title">عنوان</label>
                            <input type="text" class="form-control" name="title" id="title"
                                   value="<?= $post->title; ?>">
                        </section>
                        <section class="form-group">
                            <label for="image">تصویر</label>
                            <div class="row form-row">
                                <input type="file" class="form-control" style="width: 45%;" name="image" id="image">
                                <img src="<?= asset($post->image); ?>" alt=""
                                     style="width: 100px;height : 100px;border-radius : 50%;">
                            </div>
                        </section>
                    </div>
                    <div class="row form-row">
                        <section class="form-group">
                            <label for="cat_id">دسته بندی</label>

                            <select class="form-control" name="cat_id" id="cat_id">
                                <?php
                                global $connect;
                                $query = 'SELECT * FROM blog.categories ';
                                $statement = $connect->prepare($query);
                                $statement->execute();
                                $categories = $statement->fetchAll();
                                foreach ($categories as $category) {
                                    ?>
                                    <option value="<?= $category->id; ?>" <?php if ($category->id == $post->cat_id) echo 'selected'; ?>><?= $category->name ?></option>
                                <?php } ?>
                            </select>

                        </section>
                        <section class="form-group">
                            <label for="body">بدنه</label>
                            <textarea class="form-control" name="body" id="body"
                                      rows="10"><?= $post->body; ?></textarea>
                        </section>
                    </div>
                    <section class="form-group float-right">
                        <button type="submit" class="btn btn-primary m-1">ویرایش</button>
                        <button type="submit" class="btn btn-danger m-1">انصراف</button>
                    </section>
                </form>

            </section>
        </section>
    </section>

</section>

<script src="../../asset/js/jquery.min.js"></script>
<script src="../../asset/js/bootstrap.min.js"></script>
</body>

</html>