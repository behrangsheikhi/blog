<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

global $connect;

$select = 'SELECT * FROM blog.users WHERE id = ? ';
$statement = $connect->prepare($select);
$statement->execute([$_GET['user_id']]);
$user = $statement->fetch();

if ($user === false)
{
    redirect('admin/users');
}

// update the record
if (
    isset($_POST['first_name']) and $_POST['first_name'] !== ''
    and isset($_POST['last_name']) and $_POST['last_name'] !== ''
    and isset($_POST['username']) and $_POST['username'] !== ''
    and isset($_POST['email']) and $_POST['email'] !== ''
    and isset($_POST['password']) and $_POST['password'] !== ''
)
{
    $query = 'SELECT * FROM blog.users WHERE id = ? ;';
    $statement = $connect->prepare($query);
    $statement->execute([$_GET['user_id']]);
    $user = $statement->fetch();

    if (isset($_FILES['image']) and $_FILES['image']['name'] !== '')
    {
        $allowedMimes = ['jpg', 'jpeg', 'gif', 'png'];
        $imageMime = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

        if (!in_array($imageMime, $allowedMimes))
        {
            redirect('admin/users');
        }
//        D:XAMPP\htdocs\my_blog or localhost\my_blog
        $basePath = dirname(__DIR__, 2);
        if (file_exists($basePath . $user->image))
        {
            unlink($basePath . $user->image);
        }

        $image = '/asset/images/users/' . date("y_m_d_H_i_s") . '.' . $imageMime;
        $uploadImage = move_uploaded_file($_FILES['image']['tmp_name'], $basePath . $image);

        if ($user !== false and $uploadImage !== false)
        {
            $query = 'UPDATE blog.users SET first_name = ?, last_name = ?, username = ?,email = ?, password = ?,image = ?, updated_at = NOW() WHERE id = ? ;';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['first_name'], $_POST['last_name'], $_POST['username'], $_POST['email'], $_POST['password'], $image, $_GET['user_id']]);
        }
        else
        {
            redirect('admin/users');
        }
    }
    else
    {
        if ($user !== false)
        {
            $sql = 'UPDATE blog.users SET first_name = ?, last_name = ?, username = ?, email = ?, password = ?, updated_at = NOW() WHERE id = ? ;';
            $statement = $connect->prepare($sql);
            $statement->execute([$_POST['first_name'], $_POST['last_name'], $_POST['username'], $_POST['email'], $_POST['password'], $_GET['user_id']]);
        }
    }

    redirect('admin/users');
}

?>


<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>ویرایش کاربر</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>">

</head>

<body>
<section id="app">
    <?php require_once '../layouts/top-nav.php'; ?>
    <section class="container-fluid">

        <section class="row">
            <section class="col-md-2 p-0">
                <?php require_once '../layouts/sidebar.php'; ?>
            </section>
            <section class="col-md-10 pt-3">
                <h3 class="page-title">
                    ویرایش حساب کاربری
                </h3>
                <form class="cat_create_form" action="<?= url('admin/users/edit.php?user_id=') . $_GET['user_id']; ?>"
                      method="post" enctype="multipart/form-data">
                    <div class="row form-row">
                        <section class="form-group">
                            <label for="first_name" class="float-right d-block">نام</label>
                            <input type="text" class="form-control d-inline" name="first_name" id="first_name"
                                   value="<?= $user->first_name; ?>">
                        </section>
                        <section class="form-group">
                            <label for="last_name" class="float-right d-block">نام خانوادگی</label>
                            <input type="text" class="form-control d-inline" name="last_name" id="last_name"
                                   value="<?= $user->last_name; ?>">
                        </section>
                    </div>
                    <div class="row form-row">
                        <section class="form-group">
                            <label for="username" class="float-right d-block">نام کاربری</label>
                            <input type="text" class="form-control d-inline" name="username" id="username"
                                   value="<?= $user->username; ?>">
                        </section>
                        <section class="form-group">
                            <label for="email" class="float-right d-block">ایمیل</label>
                            <input type="text" class="form-control d-inline" name="email" id="email"
                                   value="<?= $user->email; ?>">
                        </section>
                    </div>
                    <div class="row form-row">

                        <section class="form-group">
                            <label for="password" class="float-right d-block">رمز عبور</label>
                            <input type="password" class="form-control d-inline" name="password" id="password"
                                   value="<?=
                                        $user->password;
                                   ?>">
                        </section>
                        <section class="form-group">
                            <label for="password_confirm" class="float-right d-block">تایید رمز عبور</label>
                            <input type="password" class="form-control d-inline" name="password_confirm" id="password"
                                   value="
                                   <?=
                                        $user->password;
                                   ?>">
                        </section>
                    </div>
                    <div class="row form-row mb-3" style="width: 50%;">
                        <label for="image">تصویر</label>
                        <input type="file" class="form-control" name="image" id="image">
                        <img src="<?= asset($user->image); ?>" alt=""
                             style="width: 100px;height : 100px;border-radius : 50%;">
                    </div>
                    <section class="form-group">
                        <button type="submit" class="btn btn-primary float-right m-1">ویرایش</button>
                        <button type="submit" class="btn btn-danger float-right m-1" value="<?= url('admin/users') ?>">
                            انصراف
                        </button>
                    </section>

                </form>

            </section>
        </section>
    </section>

</section>

<script src="../../asset/js/jquery.min.js"></script>
<script src="../../asset/js/bootstrap.min.js"></script>
</body>

</html>