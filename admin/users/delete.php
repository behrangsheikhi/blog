<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';


if (isset($_GET['user_id']) and $_GET['user_id'] !== '') {
    global $connect;

    $delete = "DELETE FROM blog.users WHERE id = ? ";
    $statement = $connect->prepare($delete);
    $statement->execute([$_GET['user_id']]);

}
redirect('admin/users');