<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>اعضا</title>
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>" media="all" type="text/css">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>" media="all" type="text/css">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>" media="all" type="text/css">
</head>

<body>
    <section id="app">
        <?php require_once '../layouts/top-nav.php'; ?>
        <section class="container-fluid">
            <section class="row">
                <section class="col-md-2 p-0">
                    <?php require_once '../layouts/sidebar.php'; ?>
                </section>
                <section class="col-md-10 pt-3">

                    <section class="mb-2 d-flex justify-content-between align-items-center">
                        <h3 class="page-title">
                            اعضا 
                        </h3> 

                    </section>

                    <section class="table-responsive">
                        <table class="table table-striped table-">
                            <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>تصویر</th>
                                    <th>نام</th>
                                    <th>نام خانوادگی</th>
                                    <th>نام کاربری</th>
                                    <th>ایمیل</th>
                                    <th>تاریخ عضویت</th>
                                    <th>تاریخ بروزرسانی</th>
                                    <th>تنظیمات</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                global $connect;
                                $query = "SELECT * FROM blog.users";
                                $statement = $connect->prepare($query);
                                $statement->execute();
                                $users = $statement->fetchAll();
                                foreach ($users as $user) {
                                ?>
                                    <tr>
                                        <td><?= $user->id; ?> </td>
                                        <td><img alt="user-image" class="user_image" src="<?php
                                              $basePath = dirname(__DIR__,1);
                                              $default_image =BASE_URL.'/asset/images/system/user.png';
                                            if($user->image){
                                                    echo BASE_URL.$user->image;
                                                }
                                                if(!$user->image){
                                                    echo $default_image;
                                                }
                                            ?>" ></td>
                                        <td><?= $user->first_name; ?> </td>
                                        <td><?= $user->last_name; ?> </td>
                                        <td><?= $user->username; ?> </td>
                                        <td><?= $user->email; ?> </td>
                                        <td><?= $user->created_at; ?> </td>
                                        <td><?php if (empty($user->updated_at)) {
                                                echo "-";
                                            } else {
                                                echo $user->updated_at;
                                            } ?></td>

                                        <td class="btn-box">
<!--                                            here was edit button-->

                                            <a href="<?= url('admin/users/delete.php?user_id=') . $user->id; ?>" class="btn btn-danger btn-sm">حذف</a>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </section>


                </section>
            </section>
        </section>





    </section>

    <script src="<?= asset('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= asset('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>