<link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
<link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>">
<nav class="navbar navbar-expand-lg navbar-dark bg-blue">
  <a class="navbar-brand" href="">پنل مدیریت</a>
  <section class="collapse navbar-collapse" id="navbarSupportedContent"></section>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent ">
        <ul class="navbar-nav mr-auto align-items-center">
            <li class="nav-item active">
                <label for="search-box">
                    <input id="search-box" type="text" placeholder="جستجو . . ."><a href="#"><i class="fa fa-search"></i></a>
                </label>
            </li>
<!--            <a class="nav-link" href=" ">صفحه اصلی <span class="sr-only">(current)</span></a>-->
<!--            <li class="nav-item">-->
<!--                <a class="nav-link" href=" ">خروج</a>-->
<!--            </li>-->
        </ul>
    </div>
</nav>

<script src="<?= asset('asset/js/jquery.min.js') ?>"></script>
<script src="<?= asset('asset/js/bootstrap.min.js') ?>"></script>