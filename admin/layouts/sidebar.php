
<section class="sidebar">
    <section class="sidebar-link">
        <a href="<?= url('app/blog.php') ?>">صحفه اصلی سایت</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="<?= url('admin/category') ?>">دسته بندی</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="<?= url('admin/post') ?>">پست</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="<?= url('admin/users') ?>">اعضا</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="#"> پیام ها</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="">تماس با ما</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="">پشتیبانی</a>
    </section>
    <hr>
    <section class="sidebar-link">
        <a href="<?=url('auth/logout.php')?>">خروج از حساب</a>
    </section>

</section>