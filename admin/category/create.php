<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

if(isset($_POST['name']) and $_POST['name'] !== ''){

    global $connect;

    $insert = "INSERT INTO blog.categories SET name = ?,created_at = NOW() ;";
    $statement = $connect->prepare($insert);
    $statement->execute([$_POST['name']]);

    redirect('admin/category');
}

?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>پنل مدیریت</title>

    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>">

</head>

<body>
    <section id="app">
        <?php require_once '../layouts/top-nav.php'; ?>
        <section class="container-fluid">

            <section class="row">
                <section class="col-md-2 p-0">
                    <?php require_once '../layouts/sidebar.php'; ?>
                </section>
                <section class="col-md-10 pt-3">
                <h3 class="page-title">
                ایجاد دسته بندی
            </h3>
                    <form class="cat_create_form" action="<?= url('admin/category/create.php') ?>" method="post">
                        <section class="form-group">
                            <label for="name" class="float-right d-block font-weight-bold">نام</label>
                            <input type="text" class="form-control d-inline" name="name" id="name" placeholder="نام دسته بندی مورد نظر را وارد کنید">
                        </section>
                        <section class="form-group">
                            <button type="submit" class="btn btn-primary float-right">ایجاد</button>
                        </section>

                    </form>

                </section>
            </section>
        </section>

    </section>

    <script src="../../asset/js/jquery.min.js"></script>
    <script src="../../asset/js/bootstrap.min.js"></script>
</body>

</html>