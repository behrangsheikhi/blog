<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

global $connect;

if (!isset($_GET['cat_id'])) {
    redirect('admin/category');
}

$query = 'SELECT * FROM blog.categories WHERE id = ?';
$statement = $connect->prepare($query);
$statement->execute([$_GET['cat_id']]);
$category = $statement->fetch();
if ($category === false) {
    redirect('admin/category');
}

if (isset($_POST['name']) && $_POST['name'] !== '') {

    $query = 'UPDATE blog.categories SET name = ?, updated_at = NOW() WHERE id = ? ;';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['name'], $_GET['cat_id']]);
    redirect('admin/category');
}

?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>ویرایش دسته بندی</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>" media="all" type="text/css">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>" media="all" type="text/css">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>" media="all" type="text/css">
</head>

<body>
    <section id="app">
        <?php require_once '../layouts/top-nav.php'; ?>

        <section class="container-fluid">
            <section class="row">
                <section class="col-md-2 p-0">
                    <?php require_once '../layouts/sidebar.php'; ?>

                </section>
                <section class="col-md-10 pt-3">
                    <h3 class="page-title">
                        ویرایش دسته بندی 
                    </h3>
                    <form action="<?= url('admin/category/edit.php?cat_id=') . $_GET['cat_id'] ?>" method="post">
                        <section class="form-group">
                            <label for="name" class="float-right font-weight-bold">نام دسته بندی</label>
                            <input type="text" class="form-control" name="name" id="name" value="<?= $category->name ?>">
                        </section>
                        <section class="form-group btn-box float-right">
                            <button type="submit" class="btn btn-primary m-1">ویرایش</button>
                            <button type="submit" class="btn btn-danger m-1" value="<?= url('admin/category') ?>">انصراف</button>
                        </section>

                    </form>

                </section>
            </section>
        </section>

    </section>


    <script src="<?= asset('asset/js/jquery.min.js') ?>"></script>
    <script src="<?= asset('asset/js/bootstrap.min.js') ?>"></script>
</body>

</html>