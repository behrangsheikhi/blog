<?php
require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

?>

<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>دسته بندی ها</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css')  ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css')  ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicon-regular-rounded.css')  ?>">

</head>

<body>
    <section id="app">
        <?php require_once '../layouts/top-nav.php'; ?>
        <section class="container-fluid">
            <section class="row">
                <section class="col-md-2 p-0">
                    <?php require_once '../layouts/sidebar.php'; ?>

                </section>
                <section class="col-md-10 pt-3">
                    <section class="mb-2 d-flex justify-content-between align-items-center">
                        <h3 class="page-title">
                            دسته بندی ها 
                        </h3> 
                        <a href="<?= url('admin/category/create.php') ?>" class="btn btn-sm btn-success">ساخت دسته بندی</a>
                    </section>

                    <section class="table-responsive">
                        <table class="table table-striped table-">
                            <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام</th>
                                    <th>تاریخ ساخت</th>
                                    <th>تاریخ بروزرسانی</th>
                                    <th>تنظیمات</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                global $connect;
                                $query = "SELECT * FROM blog.categories";
                                $statement = $connect->prepare($query);
                                $statement->execute();
                                $categories = $statement->fetchAll();

                                foreach ($categories as $category) {
                                ?>
                                    <tr>
                                        <td><?= $category->id; ?></td>
                                        <td><?= $category->name; ?></td>
                                        <td><?= $category->created_at; ?></td>
                                        <td><?php if (empty($category->updated_at)) {
                                                echo "-";
                                            } else {
                                                echo $category->updated_at;
                                            } ?></td>
                                        <td class="btn-box">
                                            <a href="<?= url('admin/category/edit.php?cat_id=') . $category->id; ?>" class="btn btn-info btn-sm">ویرایش</a>
                                            <a href="<?= url('admin/category/delete.php?cat_id=') . $category->id; ?>" class="btn btn-danger btn-sm">حذف</a>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </section>


                </section>
            </section>
        </section>

    </section>

    <script src="<?= asset('../../asset/js/jquery.min.js') ?>"></script>
    <script src="<?= asset('../../asset/js/bootstrap.min.js') ?>"></script>
</body>

</html>