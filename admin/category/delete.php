<?php

require_once '../../function/helpers.php';
require_once '../../function/pdo_connection.php';
require_once '../../function/check-login.php';

if (isset($_GET['cat_id']) and $_GET['cat_id'] !== '') {
    global $connect;

    $delete = "DELETE FROM blog.categories WHERE id = ? ";
    $statement = $connect->prepare($delete);
    $statement->execute([$_GET['cat_id']]);

}

redirect('admin/category');