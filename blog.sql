-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2022 at 04:58 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'موبایل1', '2022-08-13 21:42:16', '2022-08-16 02:39:33'),
(3, 'تبلت و کتابخوان', '2022-08-13 21:43:06', NULL),
(4, 'دوربین عکاسی و فیلمبرداری', '2022-08-13 21:43:06', NULL),
(9, 'لوازم جانبی دوربین عکاسی و فیلم برداری', '2022-08-14 16:35:19', NULL),
(12, 'پوشاک', '2022-08-15 18:26:40', '2022-08-15 21:25:06');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `image` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `cat_id`, `status`, `image`, `created_at`, `updated_at`) VALUES
(8, 'پست دوم', 'بدنه پست آزمایشی', 4, 1, '/asset/images/posts/22_08_15_23_51_46.jpg', '2022-08-16 02:21:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) NOT NULL,
  `username` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `image`, `email`, `password`, `first_name`, `last_name`, `username`, `created_at`, `updated_at`) VALUES
(16, '/asset/images/users/mahnaz.png', 'mahnaz@yahoo.com', '111', 'مهناز2', 'یوسف پور', 'mahnaz', '2022-08-16 15:42:02', '2022-08-18 00:26:43'),
(17, '/asset/images/users/22_08_17_22_35_47.png', 'shabani@gmail.com', '222', 'علی', 'شبانی', 'aligbg', '2022-08-16 15:42:40', '2022-08-18 01:05:47'),
(18, '/asset/images/users/hamed.png', 'gholami@gmail.com', '555', 'حامد', 'غلامی', 'hamed', '2022-08-16 16:51:35', NULL),
(19, '/asset/images/users/grrgweqgrg.png', 'rgergwrgrgqg', '000', 'gergerger', 'rgregergeg', 'grrgweqgrg', '2022-08-17 15:35:06', NULL),
(20, '/asset/images/users/hassan.jpg', 'abdi@gmail.com', '111', 'حسن', 'عبدی', 'hassan', '2022-08-17 15:42:48', NULL),
(21, '/asset/images/users/ALIREZA.png', 'sheikhi@gmail.com', '999', 'علیرضا', 'شیخی کشتیبان', 'ALIREZA', '2022-08-17 16:04:49', NULL),
(22, '/asset/images/users/22_08_17_22_18_27.png', 'hasani@gmail.com', '555', 'شهرام', 'حسنی ', 'shahram', '2022-08-18 00:32:39', '2022-08-18 00:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `users-all`
--

CREATE TABLE `users-all` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users-all`
--

INSERT INTO `users-all` (`id`, `email`, `password`, `first_name`, `last_name`, `username`, `created_at`, `updated_at`) VALUES
(1, 'behisheikhi990@gmail.com', '$2y$10$ldATzD/nrw/S1eyHwlwicuonwDr2jRjvDIABRqGSyYJKVjOWGsMam', 'بهرنگ', 'شیخی', 'behrang', '2022-08-19 01:36:56', '0000-00-00 00:00:00'),
(2, 'alishabani@gmail.com', '$2y$10$EkUGeHt/QpP9r44M8axC1uVLZ8ijx3VaQlN82T32sC1QuObgy6/O2', 'علی', 'شبانی', 'ali', '2022-08-19 02:52:58', '0000-00-00 00:00:00'),
(3, 'hamid@gmail.com', '$2y$10$4wRuMFU5ZrsdvrrqZ8oAX./EYECm9ba5mlO9tviDx2bM5ja6UmPdm', 'حمید', 'معصومی', 'hamid', '2022-08-19 02:58:10', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users-all`
--
ALTER TABLE `users-all`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users-all`
--
ALTER TABLE `users-all`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
