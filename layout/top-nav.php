<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>بلاگ</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>>">
</head>

<body>


<nav class="navbar navbar-expand-lg bg-blue">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="<?= url('app/blog.php'); ?>">
                صفحه اصلی</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                خدمات ما</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                بلاگ</a>
        </li>
        <?php
        if (!isset($_SESSION['user'])) {
            ?>
            <li class="nav-item">
                <a href="<?= url('auth/login.php') ?>" class="nav-link">ورود</a>
            </li>
            <li class="nav-item">
                <a href="<?= url('auth/register.php') ?>" class="nav-link">ثبت نام</a>
            </li>
        <?php } else { ?>
            <li class="nav-item">
                <a href="<?= url('auth/logout.php') ?>" class="nav-link">خروج</a>
            </li>
        <?php } ?>

        <?php
        global $connect;
        $query = 'SELECT * FROM blog.categories ';
        $statement = $connect->prepare($query);
        $statement->execute();
        $categories = $statement->fetchAll();
        foreach ($categories as $category) {
            ?>
            <li class="nav-item">
                <a href="<?= url('app/category.php?cat_id='.$category->id); ?>" class="nav-link"><?= $category->name ?></a>
            </li>
        <?php } ?>
    </ul>
    <div class="collapse navbar-collapse" id="navbarSupportedContent ">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <label for="search-box">
                    <input id="search-box" type="text" placeholder="جستجو . . ."><a href="#"><i
                                class="fa fa-search"></i></a>
                </label>
            </li>
            <li class="nav-item">
                <a class="nav-link" href=" "></a>
            </li>
        </ul>
    </div>
</nav>


<script src="<?= asset('asset/js/jquery.min.js') ?>>"></script>
<script src="<?= asset('asset/js/bootstrap.min.js') ?>>"></script>
<script src="<?= asset('asset/js/script.js') ?>>"></script>

</body>

</html>