<?php
require_once '../function/helpers.php';
require_once '../function/pdo_connection.php';

global $connect;

$error = '';


if(
        isset($_POST['first_name']) and $_POST['first_name'] !== ''
    and isset($_POST['last_name']) and $_POST['last_name'] !== ''
    and isset($_POST['username']) and $_POST['username'] !== ''
    and isset($_POST['email']) and $_POST['email'] !== ''
    and isset($_POST['password']) and $_POST['password'] !== ''
    and isset($_POST['confirm']) and $_POST['confirm'] !== ''
)
{
    if($_POST['password'] === $_POST['confirm'])
    {
        if(strlen($_POST['password']) > 5)
        {
            $password = password_hash($_POST['password'],PASSWORD_DEFAULT);
            $query = 'SELECT * FROM blog.users WHERE username = ?';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['username']]);
            $username = $statement->fetch();
            if($username === false)
            {
                $query = 'SELECT * FROM blog.users WHERE email = ? ';
                $statement = $connect->prepare($query);
                $statement->execute([$_POST['email']]);
                $user_email = $statement->fetch();
                if($user_email === false)
                {
                    if(isset($_FILES['image']) and $_FILES['image']['name'] !== '')
                    {
                        $allowedMimes = ['png', 'jpg', 'jpeg', 'gif'];
                        $imageMime = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                        if (!in_array($imageMime, $allowedMimes))
                        {
                            $error = 'فایل شما معتبر نیست، لطفا از فرمت های مجاز استفاده کنید';
                        }
                        $basePath = dirname(__DIR__, 1); // D:\XAMPP\htdocs\my_blog
                        $image = '/asset/images/users/' . $_POST['username'] . '.' . $imageMime;
                        $image_upload = move_uploaded_file($_FILES['image']['tmp_name'], $basePath . $image);
                        if ($image_upload)
                        {
                            $query = 'INSERT INTO blog.users SET first_name = ? , last_name = ? , username = ? , email = ? ,password = ?, image = ?, created_at = NOW();';
                            $statement = $connect->prepare($query);
                            $statement->execute([$_POST['first_name'], $_POST['last_name'], $_POST['username'],$_POST['email'],$password, $image]);
                            redirect('auth/login.php');
                        }
                        else
                        {
                            $error = 'عملیات ناموفق';
                        }
                    }
                    else if(!isset($_FILES['image']) or $_FILES['image']['name'] === '')
                    {
                        $query = 'INSERT INTO blog.users SET first_name = ? , last_name = ? , username = ? , email = ? ,password = ?, created_at = NOW();';
                        $statement = $connect->prepare($query);
                        $statement->execute([$_POST['first_name'], $_POST['last_name'], $_POST['username'],$_POST['email'],$password]);
                        redirect('auth/login.php');
                    }
                }
                else
                {
                    $error = 'ایمیل تکراری است';
//                    redirect('auth/register.php');
                }
            }
            else
            {
                $error = 'نام کاربری قبلا استفاده شده است';
//                redirect('auth/register.php');
            }

        }
        else
        {
            $error = 'رمز عبور باید بیش از پنج کاراکتر باشد';
        }
    }
    else
    {
        $error = 'رمز عبور مطابقت ندارد';
    }

}
else{
    if(!empty($_POST)){
        $error = 'همه فیلد ها اجباری می باشند';
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ثبت نام </title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--     css files linked here-->
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>">
    <!--    end css files -->
</head>

<body>
<section id="app">
    <?php require_once '../layout/top-nav.php'; ?>
    <section style="height: 100vh; background-color: #138496" class="d-flex justify-content-center align-items-center">
        <section class="register" >
            <h1 class="bg-warning rounded-top px-2 mb-0 py-3 h5">
                ثبت نام
            </h1>
            <section class="bg-light my-0 px-2 error pt-3">
                <small class="text-danger">
                    <b>
                        <?php if ($error !== '') echo '<i class="fa fa fa-exclamation-circle"></i>' . ' '. $error ?>
                    </b>
                </small>
            </section>
            <form class="pt-3 pb-1 px-2 bg-light rounded-bottom" action="<?= url('auth/register.php') ?>" method="post" enctype="multipart/form-data">

                <div class="row form-row">
                    <section class="form-group">
                        <label for="first_name">نام</label>
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="نام . . ."/>
                    </section>
                    <section class="form-group">
                        <label for="last_name">نام خانوادگی</label>
                        <input type="text" class="form-control" name="last_name" id="last_name"
                               placeholder="نام خانوادگی . . ."/>
                    </section>
                </div>
                <div class="row form-row">
                    <section class="form-group">
                        <label for="username">نام کاربری</label>
                        <input type="text" class="form-control" name="username" id="username"
                               placeholder="نام کاربری . . ."/>
                    </section>
                    <section class="form-group">
                        <label for="email">ایمیل</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="ایمیل. . . "/>
                    </section>
                </div>
                <div class="row form-row">
                    <section class="form-group">
                        <label for="password">رمز</label>
                        <input type="password" class="form-control" name="password" id="password"
                               placeholder="رمز عبور . . ."/>
                    </section>
                    <section class="form-group">
                        <label for="confirm">تایید رمز</label>
                        <input type="password" class="form-control" name="confirm" id="confirm"
                               placeholder="تایید رمز عبور . . ."/>
                    </section>
                </div>

                <div class="row form-row">
                    <section class="form-group">
                        <label for="image" >تصویر</label>
                        <input type="file" class="form-control" name="image" id="image">
                    </section>
                </div>
                <section class="mt-4 mb-2 d-flex justify-content-between">
                    <input type="submit" class="btn btn-success btn-sm" value="ثبت نام"/>
                    <a class="text-decoration-none font-weight-bold" href="<?= url('auth/login.php') ?>">
                        <small><b>قبلا ثبت نام کرده اید؟وارد شوید</b></small>
                    </a>
                </section>
            </form>
        </section>
    </section>
</section>
<script src="../asset/js/jquery.min.js"></script>
<script src="../asset/js/bootstrap.min.js"></script>
</body>

</html>