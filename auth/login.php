<?php

session_start();

global $connect;

require_once '../function/helpers.php';
require_once '../function/pdo_connection.php';

$error = '';

if(isset($_SESSION['user'])){
    unset($_SESSION['user']);
}
if (
    isset($_POST['email']) and $_POST['email'] !== ''
    and isset($_POST['password']) and $_POST['password'] !== ''
) {
    $query = 'SELECT * FROM blog.`users` WHERE email = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['email']]);
    $user = $statement->fetch();

    if($user !== false){
        if(password_verify($_POST['password'], $user->password)){
            $_SESSION['user'] = $user->email;
            redirect('admin');
        }
        else{
            $error = 'رمز عبور اشتباه است';
        }
    }
    else{
        $error = 'ایمیل وارد شده اشتباه است';
    }
}
else{
    if(!empty($_POST)){
        $error = 'همه فیلد ها اجباری می باشند';
    }
}
?>

<!DOCTYPE html>
<html lang="en" class="login-page">

<head>
    <title>ورود اعضا</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>">
</head>

<body>
<section id="app">
    <?php require_once '../layout/top-nav.php'; ?>
    <section style="height: 100vh; background-color: #138496" class="d-flex justify-content-center align-items-center">
        <section class="login" style="width: 20rem">
            <h1 class="bg-warning rounded-top px-2 mb-0 py-3 h5">ورود </h1>
            <section class="bg-light my-0 px-2 error pt-3">
                <small class="text-danger">
                    <b>
                        <?php if ($error !== '') echo '<i class="fa fa fa-exclamation-circle"></i>' . ' ' . $error ?>
                    </b>
                </small>
            </section>
            <form class="pt-3 pb-1 px-2 bg-light rounded-bottom" action="" method="post">
                <section class="form-group">
                    <label for="email">ایمیل</label>
                    <input type="email" class="form-control" name="email" id="email" autofocus autocomplete="off"
                           placeholder="ایمیل. . . "/>
                </section>
                <section class="form-group">
                    <label for="password">رمز عبور</label>
                    <input type="password" class="form-control" name="password" id="password"
                           placeholder="رمز عبور. . . "/>
                </section>
                <section class="form-group checkbox d-flex align-items-center">
                    <input type="checkbox" class="form-control" value="lsRememberMe" name="remember-me"
                           id="remember-me"/>
                    <label for="remember-me" class="mt-2 mr-1 d-inline-block">مرا به خاطر بسپار</label>
                </section>
                <section class="mt-4 mb-2 d-flex justify-content-around mb-4">
                    <input type="submit" class="btn btn-success btn-sm" onclick="lsRememberMe()" value="ورود"/>

                    <a class="font-weight-bold text-decoration-none" href="<?= url('auth/register.php'); ?>">
                        <small><b>ثبت نام نکرده اید؟همین حالا ثبت نام کنید</b></small>
                </section>
                <section class="form-group ">
                    <a href="#" class="google-logo-section">
                        <span class="login-with-google">ورود با گوگل</span>
                        <img src="<?= asset('asset/images/system/google-logo.png') ?>" alt="google" class="google-logo">
                    </a>
                </section>

            </form>
        </section>
    </section>
</section>
<script src="<?= asset('asset/js/jquery.min.js') ?>"></script>
<script src="<?= asset('asset/js/bootstrap.min.js') ?>"></script>
<script src="<?= asset('asset/js/script.js') ?>"></script>
</body>

</html>