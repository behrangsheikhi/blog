function lsRememberMe() {
    if (document.getElementById('remember-me').checked) {
        localStorage.setItem('lsRememberMe', 'true');
    } else {
        localStorage.removeItem('lsRememberMe');
    }
}
