<?php
require_once '../function/helpers.php';
require_once '../function/pdo_connection.php';
?>
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <title>دسته بندی | بلاگ</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/uicons-regular-rounded.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
</head>
<body>
<section id="app">
    <?php require_once '../layout/top-nav.php'; ?>
    <section class="container my-5">
        <?php

        global $connect;
        $notFound = false;
        if (isset($_GET['cat_id']) and $_GET['cat_id'] !== '') {
            $query = 'SELECT * FROM blog.categories WHERE id = ? ';
            $statement = $connect->prepare($query);
            $statement->execute([$_GET['cat_id']]);
            $category = $statement->fetch();
            if ($category !== false) {
                ?>
                <section class="row cat_name">
                    <section class="col-12">
                        <h1><?= $category->name; ?></h1>
                        <hr>
                    </section>
                </section>
                <section class="row">

                <?php
                $query = 'SELECT * FROM blog.posts WHERE status=1 AND cat_id = ?';
                $statement = $connect->prepare($query);
                $statement->execute([$_GET['cat_id']]);
                $posts = $statement->fetchAll();
                foreach ($posts

                         as $post) {
                    ?>
                    <section class="col-md-4">
                        <section class="mb-2 overflow-hidden" style="max-height: 15rem;">
                            <img class="img-fluid" src="<?= asset($post->image); ?>" alt="">
                        </section>
                        <h2 class="h5 text-truncate"><?= $post->title; ?></h2>
                        <p><?= substr($post->body, 0, 100); ?></p>
                        <p><a class="btn btn-primary" href="<?= url('app/detail.php?post_id=' . $post->id); ?>"
                              role="button">مشاهده جزئیات »</a></p>
                    </section>


                    <?php
                }
            } else {
                $notFound = true;
            }
        }
        ?>

        <?php
            if ($notFound === true){
        ?>
        <section class="row">
            <section class="col-12">
                <h1>دسته بندی یافت نشد</h1>
            </section>
        </section>
        <?php } ?>
    </section>
</section>


<script src="<?= asset('asset/css/jquery.min.js') ?>"></script>
<script src="<?= asset('asset/css/bootstrap.min.js') ?>"></script>
<script src="<?= asset('asset/css/script.js') ?>"></script>
</body>
</html>