<?php
    require_once '../function/helpers.php';
    require_once '../function/pdo_connection.php';
?>
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>بلاگ | جزئیات</title>
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
</head>
<body>
<section id="app">
    <?php require_once '../layout/top-nav.php'; ?>
    <section class="container my-5">
        <!-- Example row of columns -->
        <section class="row">
            <section class="col-md-12 post-detail">
                <?php
                global $connect;
                $query = 'SELECT blog.posts.* ,blog.categories.name AS category_name FROM posts JOIN categories ON posts.cat_id = categories.id WHERE posts.status = 1 AND posts.id = ? ';
                $statement = $connect->prepare($query);
                $statement->execute([$_GET['post_id']]);
                $post = $statement->fetch();
                if ($post !== false) {
                    ?>
                    <h1><?= $post->title ?></h1>
                    <a href="<?= url('app/category.php?cat_id='.$post->cat_id); ?>" class="mb-2 pb-4"><?= $post->category_name ?></a>
                    <h5 class="d-flex justify-content-between align-items-center">

                        <span class="date-time"><?= $post->created_at ?></span>
                    </h5>
                    <article class="bg-article p-3"><img class="float-right mb-2 ml-2 embed-responsive"
                                                         src="<?= asset($post->image) ?>"
                                                         alt="">
                        <?= $post->body ?></article>
                <?php } else { ?>
                    <section>محتوای مورد نظر یافت نشد</section>
                <?php } ?>
            </section>
        </section>
    </section>

</section>
<script src="<?= asset('asset/js/jquery.min.js') ?>>"></script>
<script src="<?= asset('asset/js/bootstrap.min.js') ?>>"></script>
</body>
</html>