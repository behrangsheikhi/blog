<?php
require_once '../function/helpers.php';
require_once '../function/pdo_connection.php';

?>
<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <title>بلاگ</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- css files here -->
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/font-awesome.min.css') ?>">
    <!-- end css files -->
</head>

<body>
<section id="app">

    <?php require_once "../layout/top-nav.php" ?>

    <section class="container my-5">
        <!-- Example row of columns -->
        <section class="row">


            <?php
            global $connect;
            $query = 'SELECT * FROM blog.posts WHERE status = 1';
            $statement = $connect->prepare($query);
            $statement->execute();
            $posts = $statement->fetchAll();
            foreach ($posts as $post) {
                ?>
                <section class="col-md-4">
                    <section class="mb-2 overflow-hidden" style="max-height: 15rem;"><img class="img-fluid"
                                                                                          src="<?= asset($post->image) ?>"
                                                                                          alt="post image"></section>
                    <section class="post-detail mt-1">
                        <h2 class="h5 text-truncate"><?= $post->title; ?></h2>
                        <p class="post-body"><?= substr($post->body, 0, 202) . ' . . . '; ?></p>
                    </section>

                    <p><a class="btn btn-primary" href="<?= url('app/detail.php?post_id=') . $post->id ?>"
                          role="button">مشاهده جزئیات »</a></p>
                </section>
            <?php } ?>
        </section>
    </section>

</section>


<script src="<?= asset('asset/js/jquery.min.js'); ?>>"></script>
<script src="<?= asset('asset/js/bootstrap.min.js'); ?>>"></script>

</body>

</html>