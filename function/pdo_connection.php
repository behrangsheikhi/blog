<?php

global $connect;

$server = 'localhost';
$username = 'root';
$password = '';
$dbname = 'blog';

try {
    $option = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ);
    $connect = new PDO("mysql:host=$server;dbname=$dbname", $username, $password, $option);
    
    return $connect;
} catch (PDOException $e) {
    echo "Error " . $e->getMessage();
}
